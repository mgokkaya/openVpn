#!/usr/bin/python
# -*- coding: utf-8 -*-
import wx, sys, os
import random
import time
import gtk
import imaplib
import re
from monitor import *
import subprocess
from threading import *
import signal
import pyperclip

if os.name=='posix':
    import appindicator

tray_icon = None

class LoginFrame(wx.Frame):
    process=None
    ID_LOAD = wx.NewId()
    def __init__(self, parent, id, title, size, style = wx.DEFAULT_FRAME_STYLE ):
        wx.Frame.__init__(self, parent, id, title, size=size, style=style)
        self.loc = wx.IconLocation(r'image/logo1.xpm', 0)
        self.SetIcon(wx.IconFromLocation(self.loc))
        self.panel = MainPanel(self) 
        title_text = wx.StaticText(self.panel, label="NECMETTİN ERBAKAN UNIVERSTY\n     Secure Connection Service ", pos=(110, 20))
        user_mame_text = wx.StaticText(self.panel, label="User Name :", pos=(105, 85))
        font = wx.Font(10, wx.NORMAL, wx.LIGHT, wx.BOLD)
        font2 = wx.Font(9, wx.NORMAL, wx.LIGHT, wx.BOLD)
        self.user_name=wx.TextCtrl(self.panel, 1,size=(170,30),style=(wx.TE_LEFT),pos=(200,80))
        password_text = wx.StaticText(self.panel, label="Password   :", pos=(107, 125))
        self.password=wx.TextCtrl(self.panel, 1,size=(170,30),style=(wx.TE_PASSWORD),pos=(200,120))
        self.logo = wx.StaticBitmap(self.panel,size=(10,10),pos=(30,10))
        self.logo.SetBitmap(wx.Bitmap('image/logo1.png'))
        self.logo2 = wx.StaticBitmap(self.panel,size=(10,10),pos=(370,10))
        self.logo2.SetBitmap(wx.Bitmap('image/logo2.png'))
        self.login= wx.Button(self.panel,1, label="LOGIN",pos=(200,155),size=(70,30))
        title_text.SetFont(font)
        user_mame_text.SetFont(font)
        password_text.SetFont(font)
        self.Bind(wx.EVT_BUTTON, self.OnClick,self.login)
        self.cb3 = wx.CheckBox(self.panel, label = 'remember',pos = (271,160)) 
        self.cb3.SetFont(font2)
        self.Bind(wx.EVT_CHECKBOX,self.onChecked) 
        

        
    def onChecked(self,event):
        
        cb = event.GetEventObject() 
        print cb.GetValue()
        if cb.GetValue()==True:
            self.h_file=open("user.conf","r")
            self.user_name.SetValue(self.h_file.readline())
            self.password.SetValue(self.h_file.readline())
        else:
            self.user_name.SetValue("")
            self.password.SetValue("")
        self.h_file.close()

    def OnClick(self,event):
        global tray_icon
        user_file=open(r"user.conf", "w")
        user_file.write(self.user_name.GetValue())
        user_file.write('\n')
        user_file.write(self.password.GetValue())
        user_file.close()
        root_pw = wx.GetTextFromUser("Enter Root Password:")

        if os.name == 'nt':
            self.process = subprocess.Popen(['...\\OpenVPN\\bin\\openvpn.exe','--config','...\\OpenVPN\\config\\duvar-udp-1194-config.ovpn','--management','127.0.0.1','5555','--auth','-user','-pass','...\\OpenVPN\\config\\user.conf.txt'], shell=True)
            tray_icon = TaskBarIcon()
        else :
            self.process=subprocess.Popen("echo {pw} | sudo openvpn --config  .../openvpn/duvar-udp-1194-config.ovpn --management 0.0.0.0 5555 --auth-user-pass user.conf".format(pw=root_pw), shell=True,stdout=subprocess.PIPE)
            tray_icon.run1(self.process.pid)
        
        for i in range(0,2): 
            settings,vpn = cfg_default_settings()
            success=False        
            while not success:
                try:
                    state = openvpn_send_command(vpn['neu'], 'state\n')
                    success = True
                except Exception, e:
                    success = False
                    time.sleep(3)
            self.v=vpn['neu']['state'] = openvpn_parse_state(state)
            
        #print ">>>>",self.v['connected']
        if self.v['connected']!='AUTH':
            notice = wx.StaticText(self.panel, label="CORRECT PASSWORD ", pos=(200, 210))
            notice.SetForegroundColour('green')
            #time.sleep(5)
            self.Close()

            
        else:
            notice = wx.StaticText(self.panel, label="User Name or\n Password WRONG!", pos=(200, 210))
            notice.SetForegroundColour('red')


class AboutFrame(wx.Frame):
    ID_LOAD = wx.NewId()
    def __init__(self, parent, id, title, size, style = wx.DEFAULT_FRAME_STYLE ):
        wx.Frame.__init__(self, parent, id, title, size=size, style=style)
        self.loc = wx.IconLocation(r'/image/logo1.ico', 0)
        self.SetIcon(wx.IconFromLocation(self.loc))
        panel = MainPanel2(self) 
        about_file=open("about.txt","r")
        self.text = wx.StaticText(panel, label=about_file, pos=(10, 10))
class ErrorFrame(wx.Frame):
    ID_LOAD = wx.NewId()
    def __init__(self, parent, id, title, size, style = wx.DEFAULT_FRAME_STYLE ):
        wx.Frame.__init__(self, parent, id, title, size=size, style=style)
        self.loc = wx.IconLocation(r'/image/logo1.ico', 0)
        self.SetIcon(wx.IconFromLocation(self.loc))
        panel = MainPanel(self) 
        self.error_type=wx.TextCtrl(panel, 1,size=(230,30),style=(wx.TE_LEFT),pos=(100,50))
        self.error_type.SetValue('404 PAGE NOT FOUND!')
        self.send= wx.Button(panel,1, label="SEND",pos=(100,80),size=(70,30))
        self.Bind(wx.EVT_BUTTON, self.f_send,self.send)
        self.copy= wx.Button(panel,2, label="COPY",pos=(170,80),size=(80,30))
        self.Bind(wx.EVT_BUTTON, self.f_copy,self.copy)
        self.exit= wx.Button(panel,3, label="EXİT",pos=(250,80),size=(70,30))
        self.Bind(wx.EVT_BUTTON, self.f_exit,self.exit)
    def f_send(self,event):
        pass
    def f_copy(self,event):
        error=self.error_type.GetValue()
        pyperclip.copy(error)
    def f_exit(self,event):
        self.Close()
class HelpFrame(wx.Frame):
    ID_LOAD = wx.NewId()
    def __init__(self, parent, id, title, size, style = wx.DEFAULT_FRAME_STYLE ):
        wx.Frame.__init__(self, parent, id, title, size=size, style=style)
        self.loc = wx.IconLocation(r'image/logo1.ico', 0)
        self.SetIcon(wx.IconFromLocation(self.loc))
        panel = MainPanel2(self)
class ControlThread(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global tray_icon
        while True:
            settings,vpn = cfg_default_settings()
            success=False        
            status = 0
            while not success:
                try:
                    state = openvpn_send_command(vpn['neu'], 'state\n')
                    success = True
                except Exception, e:
                    success = False
                    time.sleep(3)
            self.v=vpn['neu']['state'] = openvpn_parse_state(state)
            if self.v['connected']=='CONNECTED' :
                status =  1
            elif self.v['connected']=='WAIT' or self.v['connected']=='GET_CONFIG':
                status = -1
            else:
                status =  0
            if os.name=='nt':
                tray_icon.CreatePopupMenu(status)
            else:
                tray_icon.menu_setup(status)
            time.sleep(5)
            


class TrayIcon():
    _process = None

    ct = ControlThread()
    ct.setDaemon(True)
    ct.start()
    
    def run1(self,deneme=None):
        self.veri=deneme
    def menu_setup(self,status):
        self.menu = gtk.Menu()
        print ">>>>",status
        if status ==1:
            self.ind = appindicator.Indicator("trayicon","image/green-24.png",appindicator.CATEGORY_APPLICATION_STATUS)
            self.ind.set_status(appindicator.STATUS_ACTIVE)
            self.menu = gtk.Menu()
            self.start_item=gtk.MenuItem("Disconnect")
            self.start_item.connect("activate",self.fdisconnect)
            self.start_item.show()
            self.menu.append(self.start_item)
        elif status==-1:
            self.ind = appindicator.Indicator("trayicon","image/yellow-24.png",appindicator.CATEGORY_APPLICATION_STATUS)
            self.ind.set_status(appindicator.STATUS_ACTIVE)
            self.start_item=gtk.MenuItem("Wait...")
            self.start_item.connect("activate",self.fwait)
            self.start_item.show()
            self.menu.append(self.start_item)
        else :
            self.ind = appindicator.Indicator("trayicon","image/red-24.png",appindicator.CATEGORY_APPLICATION_STATUS)
            self.ind.set_status(appindicator.STATUS_ACTIVE)
            self.start_item=gtk.MenuItem("Connect")
            self.start_item.connect("activate",self.fconnect)
            self.start_item.show()
            self.menu.append(self.start_item)

        self.about_item=gtk.MenuItem("About")
        self.about_item.connect("activate",self.about)
        self.about_item.show()
        self.menu.append(self.about_item)

        self.options_item=gtk.MenuItem("Details")
        self.options_item.connect("activate",self.details)
        self.options_item.show()
        self.menu.append(self.options_item)

        self.help_item=gtk.MenuItem("Help")
        self.help_item.connect("activate",self.help)
        self.help_item.show()
        self.menu.append(self.help_item)

        self.quit_item = gtk.MenuItem("Exit")
        self.quit_item.connect("activate", self.quit)
        self.quit_item.show()
        self.menu.append(self.quit_item)
        self.ind.set_menu(self.menu)
        #print ">>>buraya geldi<<<"

    def main(self):
        gtk.main()

    def fconnect(self):
        frame = LoginFrame(None, 1, "NEU OPEN VPN",wx.Size(500,300))
        frame.Show()
    def fwait(self):
        pass
    def fdisconnect(self,deneme2=None):
        subprocess.call("sudo kill -9 " + str(self.veri+3), shell=True)

    def about(self):
        frame_about = AboutFrame(None, 1, "NEU OPEN VPN ABOUT",wx.Size(500,300))
        frame_about.Show()
    def help(self):
        frame_help = HelpFrame(None, 1 , "NEU OPEN VPN HELP PAGE",wx.Size(1366,760))
        frame_help.Show()

    def details(self):
        pass
    def quit(self):
        sys.exit(0)

TRAY_TOOLTIP = 'NEU OPENVPN'
TRAY_NOTCONNECT = 'image\\red.png'
TRAY_CONNECT= 'image\\green.png'
TRAY_WAIT='image\\yellow.png'

class TaskBarIcon(wx.TaskBarIcon):

    ct = ControlThread()
    ct.setDaemon(True)
    ct.start()

    def __init__(self):
        super(TaskBarIcon, self).__init__()

    def CreatePopupMenu(self,status):
        menu = wx.Menu()
        if status==1:
            create_menu_item(menu, 'DISCONNECT', self.on_bye)
        elif status==-1 :
            create_menu_item(menu, 'RECONNECT',self.on_hello)
        else:
            create_menu_item(menu, 'CONNECT', self.on_hello)

        create_menu_item(menu , 'ABOUT',self.about)
        create_menu_item(menu , 'HELP',self.help)
        create_menu_item(menu, 'EXIT', self.on_exit)
        menu.AppendSeparator()
        return menu

    def set_icon(self, path):
        icon = wx.IconFromBitmap(wx.Bitmap(path))
        self.SetIcon(icon, TRAY_TOOLTIP)

    def on_hello(self, event):
        frame = LoginFrame(None, 1, "NEU OPEN VPN",wx.Size(500,300))
        frame.Show()
        self.Destroy()
        self.set_icon(TRAY_CONNECT)
    def about(self,event):
        frame_about = AboutFrameF(None, 1, "NEU Güvenli Bağlantı HAKKINDA",wx.Size(500,300))
        frame_about.Show()
    def help(selfself,event):
        frame_help = HelpFrame(None, 1 , "NEU Güvenli Bağlantı YARDIM",wx.Size(1366,760))
        frame_help.Show()
    def on_bye(self, event):
        subprocess.call("taskkill /f /im  openvpn.exe", shell=True)
        self.set_icon(TRAY_NOTCONNECT)
    def on_exit(self, event):
        subprocess.call("taskkill /f /im  openvpn.exe", shell=True)
        self.Destroy()

class MyApp(wx.App):
    def OnInit(self):
        frame = LoginFrame(None, 1, "NEU OPEN VPN",wx.Size(460,300))
        frame.Show()
        return True

def main(argv=None):
    global tray_icon
    if argv is None:
        argv = sys.argv

    if os.name == 'nt':
        tray_icon = TaskBarIcon()
    else :
        tray_icon = TrayIcon()
    app = MyApp(0)
    app.MainLoop()
    tray_icon.main()


class MainPanel(wx.Panel):
    
    def __init__(self, parent):
        
        wx.Panel.__init__(self, parent=parent)
        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
        self.frame = parent

        sizer = wx.BoxSizer(wx.VERTICAL)
        hSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.SetSizer(hSizer)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.arkaplan)

    def arkaplan(self, evt):
       
        dc = evt.GetDC()

        if not dc:
            dc = wx.ClientDC(self)
            rect = self.GetUpdateRegion().GetBox()
            dc.SetClippingRect(rect)
        dc.Clear()
        bmp = wx.Bitmap("image/background.jpg")
        dc.DrawBitmap(bmp, 0, 0)
class MainPanel2(wx.Panel):
    
    def __init__(self, parent):
        
        wx.Panel.__init__(self, parent=parent)
        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
        self.frame = parent

        sizer = wx.BoxSizer(wx.VERTICAL)
        hSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.SetSizer(hSizer)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.arkaplan)

    def arkaplan(self, evt):
       
        dc = evt.GetDC()

        if not dc:
            dc = wx.ClientDC(self)
            rect = self.GetUpdateRegion().GetBox()
            dc.SetClippingRect(rect)
        dc.Clear()
        bmp = wx.Bitmap("image/background.jpg")
        dc.DrawBitmap(bmp, 0, 0)
if __name__ == '__main__':
        main()
